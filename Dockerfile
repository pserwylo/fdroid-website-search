FROM debian:stretch

RUN printf "path-exclude=/usr/share/locale/*\npath-exclude=/usr/share/man/*\npath-exclude=/usr/share/doc/*\npath-include=/usr/share/doc/*/copyright\n" >/etc/dpkg/dpkg.cfg.d/01_nodoc \
	&& apt-get update \
	&& apt-get -y upgrade \
	&& apt-get -y dist-upgrade \
	&& apt-get install -y --no-install-recommends \
		python3 \
		python3-pip \
		python3-setuptools \
	&& apt-get -y autoremove --purge \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

ADD manage.py /opt/fdroid-website-search/manage.py
ADD requirements.txt /opt/fdroid-website-search/requirements.txt
ADD fdroid_website_search /opt/fdroid-website-search/fdroid_website_search

WORKDIR /opt/fdroid-website-search

RUN pip3 install -r requirements.txt
RUN pip3 install 'gunicorn<20.0'
RUN python3 manage.py migrate
RUN python3 manage.py fdroidfetchindex
RUN python3 manage.py rebuild_index --noinput -v 2 -k 1 --batch-size=100 && \
    chgrp -R nogroup /opt/fdroid-website-search && \
    chmod -R g+rX /opt/fdroid-website-search && \
    chmod g+w /opt/fdroid-website-search/whoosh_index

EXPOSE 8000
USER nobody
CMD /usr/local/bin/gunicorn fdroid_website_search.wsgi --bind 0.0.0.0:8000
