#!/usr/bin/env python3
#
# models.py - part of fdroid-website-search django application
# Copyright (C) 2017 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models

class Language(models.Model):
    name = models.CharField('name', max_length=255)
    def __str__(self):
        return self.name

class FDroidSite(models.Model):
    repoUrl = models.CharField('repository URL', max_length=255)
    siteUrl = models.CharField('website URL', max_length=255)
    def __str__(self):
        return str(self.siteUrl).replace('http://', '').replace('https://', '')

class App(models.Model):
    packageName = models.CharField('package name', max_length=255)
    name = models.CharField('name', max_length=255)
    summary = models.CharField('summary', max_length=255, blank=True)
    description = models.TextField()
    icon = models.CharField('icon', max_length=255)
    site = models.ForeignKey(FDroidSite, verbose_name="F-Droid website")
    def siteUrl(self):
        return self.site.siteUrl + '/packages/' + self.packageName
    def iconUrl(self):
        return self.site.repoUrl + '/icons/' + self.icon
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('packageName', 'site')

class AppI18n(models.Model):
    name = models.CharField('name', max_length=255, blank=True)
    summary = models.CharField('summary', max_length=255, blank=True)
    description = models.TextField(blank=True)
    whatsNew = models.TextField(blank=True)
    lang = models.ForeignKey(Language, verbose_name="language")
    entry = models.ForeignKey(App, verbose_name="package")
    class Meta():
        unique_together = ('lang', 'entry')
