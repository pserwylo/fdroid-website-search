#!/usr/bin/env python3
#
# fdroidfetchindex.py - part of fdroid-website-search django application
# Copyright (C) 2017 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from fdroid_website_search.templatetags import fdroid_website_search_tags

from fdroid_website_search.models import Language
from fdroid_website_search.models import Language
from fdroid_website_search.models import FDroidSite
from fdroid_website_search.models import App
from fdroid_website_search.models import AppI18n

from collections import defaultdict

import os
import json
import zipfile
import tempfile
import urllib.request

class Command(BaseCommand):
    def handle(self, *args, **options):

        def get_or_create_lang(name):
            try:
                return Language.objects.get(name=name)
            except ObjectDoesNotExist:
                l = Language()
                l.name = name
                l.save()
                return l

        def get_or_create_site(repo, site):
            try:
                return FDroidSite.objects.get(repoUrl=repo, siteUrl=site)
            except ObjectDoesNotExist:
                s = FDroidSite()
                s.repoUrl = repo
                s.siteUrl = site
                s.save()
                return s

        for fdroid_site_info in fdroid_website_search_tags.get_all_repos():
            #repo_url = fdroid_site_info['repo']
            #site_url = fdroid_site_info['site']
            fdroid_site = get_or_create_site(fdroid_site_info['repo'],
                                             fdroid_site_info['site'])

            with tempfile.TemporaryDirectory() as tmpdir:
                jarpath = os.path.join(tmpdir, 'index-v1.jar')
                index_url = fdroid_site.repoUrl + '/index-v1.jar'
                print("fetching '{}' ...".format(index_url))
                with urllib.request.urlopen(index_url) as i:
                    with open(jarpath, 'wb') as o:
                        o.write(i.read())
                with zipfile.ZipFile(jarpath) as z:
                    index = json.loads(z.read('index-v1.json').decode('utf-8'))

                print("updating database ...")
                for app in index['apps']:
                    try:
                        a = App.objects.get(packageName=app['packageName'])
                    except ObjectDoesNotExist:
                        a = App()
                    a.packageName = app['packageName']
                    a.name = app['name']
                    a.summary = app.get('summary', '')
                    a.icon = app.get('icon', '')
                    a.description = app.get('description', '')
                    a.site = fdroid_site
                    a.save()

                    for langName, loc in app.get('localized', {}).items():
                        l = get_or_create_lang(langName)
                        try:
                            aI18n = AppI18n.objects.get(entry=a, lang=l)
                        except ObjectDoesNotExist:
                            aI18n = AppI18n()
                        aI18n.name = loc.get('name', '')
                        aI18n.summary = loc.get('summary', '')
                        aI18n.description = loc.get('description', '')
                        aI18n.whatsNew = loc.get('whatsNew', '')
                        aI18n.entry = a
                        aI18n.lang = l
                        aI18n.save()

            print("Parsed infos of {} Apps and stored them in the local database.".format(len(index['apps'])))
        print("There are infos about {} Apps stored in the local database now.".format(len(App.objects.all())))
