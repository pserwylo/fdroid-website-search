from django import template

from fdroid_website_search import default_config


register = template.Library()

@register.assignment_tag
def get_all_repos():
    return default_config.fdroid


@register.assignment_tag
def get_color_background():
    return default_config.color_background


@register.assignment_tag
def get_color_background_header():
    return default_config.color_background_header


@register.assignment_tag
def get_color_text():
    return default_config.color_text


@register.assignment_tag
def get_link_back():
    return default_config.fdroid[0]['site']
