#!/usr/bin/env python3
#
# admin.py - part of fdroid-website-search django application
# Copyright (C) 2017 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.contrib import admin

from fdroid_website_search.models import App
from fdroid_website_search.models import AppI18n

#class AppI18nAdmin(admin.TabularInline):
class AppI18nAdmin(admin.StackedInline):
    model = AppI18n

class AppAdmin(admin.ModelAdmin):
    list_display = ('name', 'packageName')
    inlines = [AppI18nAdmin]
admin.site.register(App, AppAdmin)
