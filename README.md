NOTE: This project is still under development and not suited for production yet.
You can try the development version on: https://staging.f-droid.org/search

get development setup running:

    git clone https://gitlab.com/uniqx/fdroid-website-search
    cd fdroid-website-search

    virtualenv -p python3 env
    . env/bin/activate
    pip install -r requirements.txt

    python manage.py migrate
    python manage.py fdroidfetchindex
    python manage.py rebuild_index -v 2 -k 1 --batch-size=100
    python manage.py runserver

    curl http://localhost:8000/?q=droid

Docker is intended for deployment, you are by no means encouraged to
install that proprietary software on your system.
(The vagrant setup here supplies a docker runtime using libvirt+kvm.
You are not required to use it. It's just handy for using docker
without having to install proprietary software on your system.)

docker brain dump:

    # build docker container
    docker build -t fdroid-website-search .

    # run docker container in background
    docker run --detach --name fdroid-website-search --publish 8000:8000 fdroid-website-search

    # stop docker container and remove it
    docker stop fdroid-website-search
    docker rm fdroid-website-search
